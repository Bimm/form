export const TabNav = ({ step }) => (
  <div>
    <div className="tab">
      <div className={step === 1 ? "active-tab" : "inactive-tab"}>
        <p>User</p>
      </div>
      <div className={step === 2 ? "active-tab" : "inactive-tab"}>
        <p>Privacy</p>
      </div>
      <div className={step === 3 ? "active-tab" : "inactive-tab"}>
        <p>Done</p>
      </div>
    </div>
  </div>
);
