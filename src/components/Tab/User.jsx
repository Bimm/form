import React, { useState } from "react";

export const User = ({ nextStep, data, handleChange }) => {
  const [state, setState] = useState({
    nameError: null,
    emailError: null,
    passwordError: null,
  });

  // We can also use react-hooks form library or create a custom validation hooks. 
  // But we will use this for the purpose of this test.
  const isFormValid = () => {
    let nameError = "";
    let emailError = "";
    let passwordError = "";

    if (!data?.name) {
      nameError = "Name is required";
    }

    const emailRegex = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i;
    if (!data?.email || emailRegex.test(data?.email) === false) {
      emailError = "Invalid email address";
    }

    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{10,}$/;
    if (!data?.password || passwordRegex.test(data?.password) === false) {
      passwordError =
        "Password must contain at least 10 characters, at least one uppercase and at least one lower case";
    }

    if (emailError || nameError || passwordError) {
      setState({ nameError, emailError, passwordError });
      return false;
    }

    return true;
  };

  const checkForm = (e) => {
    e.preventDefault();
    if (isFormValid()) {
      nextStep();
    }
  };

  return (
    <form className="user-form">
      <div className="input-box">
        <label>Name</label>
        <input
          type="text"
          name="name"
          value={data?.name}
          onChange={handleChange}
          required
          autoComplete="name"
          data-testid='name'
        />
        <small className="error">{state.nameError}</small>
      </div>

      <div className="input-box">
        <label>Role</label>
        <input
          type="text"
          name="role"
          value={data?.role}
          onChange={handleChange}
          required
          autoComplete="role"
          data-testid='role'
        />
      </div>

      <div className="input-box">
        <label>E-mail</label>
        <input
          type="email"
          name="email"
          value={data?.email}
          onChange={handleChange}
          required
          autoComplete="email"
          data-testid='email'
        />
        <small className="error">{state.emailError}</small>
      </div>

      <div className="input-box">
        <label>Password</label>
        <input
          type="password"
          name="password"
          value={data?.password}
          onChange={handleChange}
          required
          autoComplete="password"
          data-testid='password'
        />
        <span className="error">{state.passwordError}</span>
      </div>
      <div className="right">
        <button onClick={checkForm} data-testid='submit-step-one'>Next</button>
      </div>
    </form>
  );
};
