import React from "react";
import Checkmark from "../CheckMark";

export const Done = () => {
  return (
    <div className="success">
      <Checkmark />
      <p>
        Please verify your email address, you should have received an email from
        us already
      </p>
    </div>
  );
};
