import React from "react";

export const Privacy = ({
  previousStep,
  nextStep,
  data,
  setData,
}) => {
  return (
    <div className="user-form">
      <div className="input-box flex2">
        <input
          type="checkbox"
          name="receiveProductUpdates"
          className="checkbox"
          checked={data?.receiveProductUpdates}
          onChange={() =>
            setData({
              ...data,
              receiveProductUpdates: !data?.receiveProductUpdates,
            })
          }
        />
        <small>Receive updates about Tray.io by email</small>
      </div>

      <div className="input-box flex2">
        <input
          type="checkbox"
          name="receiveOtherUpdates"
          value={data?.receiveOtherUpdates}
          className="checkbox"
          checked={data?.receiveOtherUpdates}
          onChange={() =>
            setData({
              ...data,
              receiveOtherUpdates: !data?.receiveOtherUpdates,
            })
          }
        />
        <small>
          Receive communication by email for other products created by the
          Tray.io team
        </small>
      </div>

      <div className="flex">
        <button
          onClick={() => {
            previousStep();
          }}
          data-testid='previous-button'
        >
          Previous
        </button>
        <button
          onClick={() => {
            nextStep();
            console.log(data);
          }}
          data-testid='submit-step-two'
        >
          Next
        </button>
      </div>
    </div>
  );
};
