import * as React from "react";

import "./index.css";

const Checkmark = () => (
  <svg
    className="check_mark"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 52 52"
  >
    <circle className="check_mark_circle" cx="26" cy="26" r="25" fill="none" />
    <path
      className="check_mark_check"
      fill="none"
      d="M14.1 27.2l7.1 7.2 16.7-16.8"
    />
  </svg>
);

export default Checkmark;
