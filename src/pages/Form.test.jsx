import { fireEvent, render, screen } from "@testing-library/react";
import Form from "./Form";

const nameText = "name";
const emailText = "email@email.com";

const goodPassword = "iAMagoodpass33";
const badPassword = "bad";
const badEmail = "email";

const nameErrorText = "Name is required";
const emailErrorText = "Invalid email address";
const passwordErrorText =
  "Password must contain at least 10 characters, at least one uppercase and at least one lower case";

const successText =
  "Please verify your email address, you should have received an email from us already";

const fillForm = () => {
  const name = screen.getByTestId(/name/i);
  const email = screen.getByTestId(/email/i);
  const password = screen.getByTestId(/password/i);

  fireEvent.change(name, {
    target: { value: nameText },
  });
  fireEvent.change(email, {
    target: { value: emailText },
  });
  fireEvent.change(password, {
    target: { value: goodPassword },
  });

  expect(name).toHaveValue(nameText);
  expect(email).toHaveValue(emailText);
  expect(password).toHaveValue(goodPassword);

  const button = screen.getByTestId("submit-step-one");
  fireEvent.click(button);

  const passwordError = screen.queryByText(passwordErrorText);
  expect(passwordError).not.toBeInTheDocument();
};

describe("form test", () => {
  it("renders the default step", async () => {
    render(<Form />);
    const text = screen.getByText(/Name/i);
    expect(text).toBeInTheDocument();
  });

  it("fills the form with the required fields and submits", async () => {
    render(<Form />);

    fillForm();

    // To assert that we're on the next page
    expect(
      screen.getByText(/Receive updates about Tray.io by email/)
    ).toBeInTheDocument();
  });

  it("fills the form with wrong values and submit", async () => {
    render(<Form />);
    const email = screen.getByTestId(/email/i);
    const password = screen.getByTestId(/password/i);

    fireEvent.change(email, {
      target: { value: badEmail },
    });
    fireEvent.change(password, {
      target: { value: badPassword },
    });

    expect(email).toHaveValue(badEmail);
    expect(password).toHaveValue(badPassword);
    // We did not fill the name input

    const button = screen.getByTestId("submit-step-one");
    fireEvent.click(button);

    const passwordError = screen.getByText(passwordErrorText);
    const emailError = screen.getByText(emailErrorText);
    const nameError = screen.getByText(nameErrorText);

    // Assert
    expect(passwordError).toBeInTheDocument();
    expect(emailError).toBeInTheDocument();
    expect(nameError).toBeInTheDocument();
    // To assert that we're not in the next page
    expect(
      screen.queryByText(/Receive updates about Tray.io by email/)
    ).not.toBeInTheDocument();
  });

  it("goes to the final page from the privacy page", async () => {
    render(<Form />);

    fillForm();

    const privacyPageText = screen.queryByText(
      /Receive updates about Tray.io by email/i
    );
    expect(privacyPageText).toBeInTheDocument();

    const button = screen.getByTestId("submit-step-two");
    fireEvent.click(button);

    expect(privacyPageText).not.toBeInTheDocument();
    expect(screen.getByText(successText)).toBeInTheDocument();
  });

  it("goes to the previous page from the privacy page", async () => {
    render(<Form />);

    fillForm();

    const privacyPageText = screen.queryByText(
      /Receive updates about Tray.io by email/i
    );
    expect(privacyPageText).toBeInTheDocument();

    const button = screen.getByTestId("previous-button");
    fireEvent.click(button);

    expect(privacyPageText).not.toBeInTheDocument();
    expect(screen.getByText('Name')).toBeInTheDocument();
  });
});
