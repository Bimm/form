import { useState } from "react";
import { User } from "../components/Tab/User";
import { Privacy } from "../components/Tab/Privacy";
import { Done } from "../components/Tab/Done";
import { TabNav } from "../components/Tab/TabNav";

const Form = () => {
  const [step, setStep] = useState(1);

  const [data, setData] = useState({
    name: "",
    role: "",
    email: "",
    password: "",
    receiveProductUpdates: false,
    receiveOtherUpdates: false,
  });

  const previousStep = () => {
    setStep(step - 1);
  };

  const nextStep = () => {
    setStep(step + 1);
  };

  function handleChange(e) {
    const { name, value } = e.target;
    setData((data) => ({ ...data, [name]: value }));
  }

  switch (step) {
    case 1:
      return (
        <>
          <TabNav step={step} />
          <User nextStep={nextStep} data={data} handleChange={handleChange} />
        </>
      );
    case 2:
      return (
        <>
          <TabNav step={step} />
          <Privacy
            previousStep={previousStep}
            nextStep={nextStep}
            data={data}
            setData={setData}
          />
        </>
      );
    case 3:
      return (
        <>
          <TabNav step={step} />
          <Done />
        </>
      );
    default:
      return <div className="App"></div>;
  }
};

export default Form;
