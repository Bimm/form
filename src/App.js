import "./index.css";
import Form from "./pages/Form";

const App = () => {
  return (
    <div className="container">
      <div className="form">
        <Form />
      </div>
    </div>
  );
};
export default App;
